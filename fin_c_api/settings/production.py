
import datetime

from .base import *

DEBUG = False
SECRET_KEY = os.environ['SECRET_KEY']

# Update TokenExpiration
JWT_AUTH['JWT_EXPIRATION_DELTA'] = datetime.timedelta(seconds=600)
JWT_AUTH['JWT_REFRESH_EXPIRATION_DELTA'] = datetime.timedelta(days=2)
JWT_AUTH['JWT_SECRET_KEY'] = SECRET_KEY

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ['DB_NAME'],
        'USER': os.environ['DB_USERNAME'],
        'PASSWORD': os.environ['DB_PASSWORD'],
        'HOST': os.environ['DB_HOST'],
        'PORT': os.environ['DB_PORT'],
    }
}

REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES'] = [
    'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
]

# TODO Specify the allowed hosts which can send a request.
ALLOWED_HOSTS = ['*']

# Celery Configs
CELERY_HOST = os.environ['CELERY_HOST']
CELERY_USER = os.environ['CELERY_HOST']
CELERY_PASSWORD = os.environ['CELERY_HOST']
CELERY_PORT = os.environ['CELERY_PORT']
CELERY_VIRTUAL_HOST = os.environ['CELERY_VIRTUAL_HOST']

CELERY_BROKER_URL = 'amqp://{user}:{password}@{host}:{port}/{virtual_host}'.format(**{
    'user': CELERY_USER,
    'password': CELERY_PASSWORD,
    'host': CELERY_HOST,
    'port': CELERY_PORT,
    'virtual_host': CELERY_VIRTUAL_HOST,
})
