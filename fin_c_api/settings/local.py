
import datetime

from .base import *

DEBUG = True
SECRET_KEY = '9#j4j0oatt5ya13704w57^9pp4ve-evcs8^f-!f%_^%f(al5-&'

# Update TokenExpiration
JWT_AUTH['JWT_EXPIRATION_DELTA'] = datetime.timedelta(days=10)
JWT_AUTH['JWT_REFRESH_EXPIRATION_DELTA'] = datetime.timedelta(days=5)
JWT_AUTH['JWT_SECRET_KEY'] = SECRET_KEY

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'fin_c',
        'USER': 'fin_c_user',
        'PASSWORD': 'root',
        'HOST': '127.0.0.1',
        'PORT': 5432,
    }
}

REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES'] = [
    'rest_framework.authentication.BasicAuthentication',
    'rest_framework_jwt.authentication.JSONWebTokenAuthentication',

]

ALLOWED_HOSTS = ['*']

# Celery Configs
CELERY_HOST = 'localhost'
CELERY_VIRTUAL_HOST = ''
CELERY_USER = 'guest'
CELERY_PASSWORD = 'guest'
CELERY_PORT = '5672'
CELERY_BROKER_URL = 'amqp://{user}:{password}@{host}:{port}/{virtual_host}'.format(**{
    'user': CELERY_USER,
    'password': CELERY_PASSWORD,
    'host': CELERY_HOST,
    'port': CELERY_PORT,
    'virtual_host': CELERY_VIRTUAL_HOST,
})
