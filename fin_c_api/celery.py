
from django.conf import settings
from celery import Celery

celery_app = Celery(broker=settings.CELERY_BROKER_URL)
