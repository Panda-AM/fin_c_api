
from django.utils.translation import ugettext_lazy as _
from rest_framework.serializers import ValidationError


def validate_csv_extension(value):
    if not value.name.endswith('.csv'):
        raise ValidationError(_('Allowed format is .csv'))
