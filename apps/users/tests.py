
import csv
import tempfile

from django.urls import reverse
from faker import Faker

from apps.core.tests import BaseTestCase


class UsersBulkUploadTestCase(BaseTestCase):

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.url = reverse('users:bulk-upload')

    def setUp(self):
        super().setUp()

    def test_valid_users_bulk_upload(self):

        valid_post_data = {'users_file': self.__generate_users_file()}
        response = self.client.post(self.url, valid_post_data)

        assert response.status_code == 200

    def test_invalid_users_bulk_upload(self):

        invalid_post_data = {'users_file': 'invalid_file'}
        response = self.client.post(self.url, invalid_post_data)

        assert response.status_code == 400

    def __generate_users_file(self):

        faker = Faker()
        users = list()
        tmp_csv_file = tempfile.NamedTemporaryFile(suffix='.csv')

        for i in range(10):
            users.append([faker.name, faker.email])

        with open(tmp_csv_file.name, 'w') as csv_file:
            writer = csv.writer(csv_file)
            writer.writerows(users)

        return tmp_csv_file
