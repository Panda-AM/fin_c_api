
import csv
import io

from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from validate_email import validate_email

from fin_c_api.celery import celery_app
from apps.core.serializer_fields import Base64FileField
from apps.users.validators import validate_csv_extension


class UsersBulkUploadSerializer(serializers.Serializer):

    def __init__(self, *args, **kwargs):

        self.validated_users = list()
        self.errors_list = list()

        super().__init__(self, *args, **kwargs)

    users_file = Base64FileField(write_only=True, validators=[validate_csv_extension, ])

    def validate_users_file(self, users_file):
        """
        Validate the received users file in case of invalid values or absent required fields.
        NOTE:This is a basic validation of the file structure and the field types before sending it to the WORKER.
        The values should be deeply validated before saving in DB.
        :param users_file: (FileField) received file object
        :return: (FileField) users_file
        """

        io_string = io.StringIO(users_file.read().decode('utf-8'))
        users_list = csv.reader(io_string)
        current_line = 1

        for user in users_list:

            self.__validate_row(user, current_line)
            first_name, last_name = self.__validate_full_name(user[0], current_line)
            email = self.__validate_email(user[1], current_line)

            if current_line not in self.errors_list:
                self.validated_users.append({
                    'first_name': first_name,
                    'last_name': last_name,
                    'email': email
                })

            current_line += 1
            # break

    def run(self):
        """
        Go through the each row of the validated file and send them to the message queue.
        :return:
        """

        celery_app.send_task('users_bulk_creator', kwargs={'users': self.validated_users})

    @property
    def get_response(self):

        result = {
            'passed_rows_count': len(self.validated_users),
            'invalid_rows_count': len(self.errors_list),
        }

        if self.errors_list:
            result['invalid_rows'] = self.errors_list

        return result

    def __validate_full_name(self, full_name, current_line):

        is_alpha = all(letter.isalpha() or letter.isspace() for letter in full_name)
        parted_full_name = full_name.split(' ')

        if not any([is_alpha, len(parted_full_name) > 1]):
            self.errors_list.append({
                'line {0}: {1} '.format(current_line, _('The "{}" is invalid full name.'.format(full_name)))
            })
            return None, None

        return parted_full_name[0], parted_full_name[1]

    def __validate_email(self, email, current_line):

        if not validate_email(email):
            self.errors_list.append({
                'line {0}: {1} '.format(current_line, _('The "{}" is invalid email address.'.format(email)))
            })
            return

        return email

    def __validate_row(self, row, current_line):

        if not len(row) == 2:
            self.errors_list.append({
                current_line: _('The full name and email address should be provided in the file.')
            })
