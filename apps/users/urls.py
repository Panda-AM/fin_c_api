
from django.conf.urls import url

from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token

from apps.users import views

urlpatterns = [

    # JWT Authentication endpoints
    url(r'^get-token/', obtain_jwt_token, name='get-token'),
    url(r'^refresh-token/', refresh_jwt_token, name='refresh-token'),
    url(r'^verify-token/', verify_jwt_token, name='verify-token'),

    url(r'^bulk-upload/', views.UsersBulkUploadAPIView.as_view(), name='bulk-upload'),
]
