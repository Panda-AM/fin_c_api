
from rest_framework import status
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response

from apps.core.views import APIView
from apps.users.serializers import UsersBulkUploadSerializer


class UsersBulkUploadAPIView(APIView):

    serializer_class = UsersBulkUploadSerializer
    permission_classes = [IsAdminUser]
    
    def post(self, request, *args, **kwargs):

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.run()
            return Response(serializer.get_response, status=status.HTTP_200_OK)
        
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
