
from rest_framework.views import APIView as BaseAPIView


class APIView(BaseAPIView):

    def get_serializer(self, *args, **kwargs):
        """
        Return the serializer instance that should be used for validating and
        de-serializing input, and for serializing output.
        NOTE: Currently we use this for SWAGGER documentation, as we need to
        specify this method in all APIView-s for the correct generation of the
        documentation.
        """

        if hasattr(self, 'serializer_class'):
            return self.serializer_class(*args, **kwargs)
