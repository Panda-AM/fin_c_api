
from django.contrib.auth.models import User
from django.conf import settings

from rest_framework.test import APITestCase
from rest_framework_jwt.serializers import JSONWebTokenSerializer


class BaseTestCase(APITestCase):

    admin_user = {
        'username': 'admin',
        'email': 'admin@c2s.com',
        'password': 'pass1234',
        'is_superuser': True,
        'is_staff': True,
    }

    def setUp(self):
        self.__set_auth_token()

    def __set_auth_token(self):

        token = self.__get_user_token()
        self.client.credentials(HTTP_AUTHORIZATION=settings.JWT_AUTH['JWT_AUTH_HEADER_PREFIX'] + ' ' + token)

    def __get_user_token(self):

        User.objects.create_superuser(**self.admin_user)

        serializer = JSONWebTokenSerializer(data=self.admin_user)
        serializer.is_valid(raise_exception=True)

        return serializer.object.get('token')
