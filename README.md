=================
FinC API
=================

-----------------
Development setup
-----------------

Install required system packages:

    $ sudo apt-get install python3-pip
    $ sudo apt-get install python-dev
    $ sudo apt-get install libpq-dev
    $ sudo apt-get install postgresql
    $ sudo apt-get install postgresql-contrib

Create www directory where project sites and environment dir

    $ mkdir /var/www && mkdir /var/envs && mkdir /var/envs/bin
    
Install virtualenvwrapper

    $ sudo pip3 install virtualenvwrapper
    $ sudo pip3 install --upgrade virtualenv
    
Add these to your bashrc virutualenvwrapper work

    export WORKON_HOME=/var/envs
    export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
    export PROJECT_HOME=/var/www
    export VIRTUALENVWRAPPER_HOOK_DIR=/var/envs/bin
    export IS_LOCAL=True
    source /usr/local/bin/virtualenvwrapper.sh
    
Create virtualenv

    $ cd /var/envs && mkvirtualenv --python=/usr/bin/python3 fin_c
    
Install requirements for a project.

    $ cd /var/www/c2s && pip install -r requirements/local.txt

Configure DB (local example)

    $ sudo su - postgres
    $ createdb fin_c
    $ psql
    $ CREATE USER fin_c_user;
    $ ALTER USER c2s PASSWORD 'root';
    $ ALTER USER c2s CREATEDB;

NOTE: To automatically activate environment and set necessary variables use the script.
 You will use the same commands which provides "manage.py" module

    $ ./scripts/manage.sh *

Run Migrations

    $ ./scripts/manage.sh migrate 

Run TESTS

    $ ./scripts/manage.sh test
